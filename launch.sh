#!/usr/bin/env bash

set -eux

#
# Launch a remote pipeline and wait for it
# 
# This needs several mandatory variable to be set
# 
# TRIGGER_TOKEN: token used to trigger the pipeline
#   This can be found in Settings>CI/CD>triger pipelines of the remote repo
# API_TOKEN: user token that has read_api access (poll the API)
#   TODO: check that if we can cleanup the remote pipeline when cancelling this job for instance
#

# ID du projet qui fait tourner les jobs à l'INRIA
PROJECT_ID=42824
# on devrait pouvoir utiliser depuis le repo sur framagit
# CI_PROJECT_URL
# CI_COMMIT_SHA
TRIGGER_PROJECT_URL=https://framagit.org/labos1point5/l1p5-vuejs
TRIGGER_COMMIT_SHA=f4fa20d1301b50da1640ca19c3dce28e74656f12

if [[ -z "$TRIGGER_TOKEN" ]]; then
    echo "TRIGGER_TOKEN must be set."
    exit 1
fi

if [[ -z "$API_TOKEN" ]]; then
    echo "API_TOKEN must be set."
    exit 1
fi

pipeline=$(curl -X POST \
           -F token=$TRIGGER_TOKEN \
           -F ref="main" \
           -F "variables[TRIGGER_COMMIT_SHA]=$TRIGGER_COMMIT_SHA" \
           -F "variables[TRIGGER_PROJECT_URL]=$TRIGGER_PROJECT_URL" \
           "https://gitlab.inria.fr/api/v4/projects/42824/trigger/pipeline")
echo $pipeline | jq .
pipeline_id=$(echo $pipeline | jq -r .id)
web_url=$(echo $pipeline | jq -r .web_url)

while true
do
    status=$(curl --header "PRIVATE-TOKEN: $API_TOKEN" "https://gitlab.inria.fr/api/v4/projects/$PROJECT_ID/pipelines/$pipeline_id" | jq -r .status)
    jobs=$(curl --header "PRIVATE-TOKEN: $API_TOKEN" "https://gitlab.inria.fr/api/v4/projects/$PROJECT_ID/pipelines/$pipeline_id/jobs")

    echo $status
    case $status in

        pending)
            echo "Pipeline is pending"
            ;;

        created)
            echo "Pipeline created"
            ;;

        success)
            echo "Pipeline success"
            exit 0
            ;;

        running)
            echo "Pipeline is running"
            ;;

        *)
            echo "Pipeline failed"
            exit 1
            ;;
    esac
    echo "Waiting before updating the pipeline status: $web_url"
    echo $jobs | jq .[].web_url
    sleep 3
done
